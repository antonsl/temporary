import UIKit

class ScrollableContentViewController: UIViewController {
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        return scrollView
    }()
    
    private var contentView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let view = self.view else {
            return
        }
        
        view.removeFromSuperview()                
        
        self.view = scrollView
        scrollView.addSubview(view)
        self.contentView = view.superview
        if let contentView = view.superview {
            view.translatesAutoresizingMaskIntoConstraints = false
            view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
            view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
            view.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
            view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        }
        let wc = view.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
        wc.priority = UILayoutPriority(rawValue: 1000)
        wc.isActive = true
        
        let hc = view.heightAnchor.constraint(equalTo: scrollView.heightAnchor)
        hc.priority = UILayoutPriority(rawValue: 700)
        hc.isActive = true
    }    
    
}
