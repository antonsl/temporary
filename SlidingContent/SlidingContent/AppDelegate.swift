//
//  AppDelegate.swift
//  SlidingContent
//
//  Created by Anton Sleptsov on 16/02/2020.
//  Copyright © 2020 Anton Sleptsov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let startViewController: MainViewController = MainViewController.storyboardInstance()
        
        window?.rootViewController = startViewController
        window?.makeKeyAndVisible()
        
        return true
    }
    
}

