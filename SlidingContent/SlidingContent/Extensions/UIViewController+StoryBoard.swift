import UIKit

extension UIViewController {
    
    static func storyboardInstance<T>() -> T {
        let storyboard = UIStoryboard(name: String(describing: self),
                                      bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: String(describing: self))
        
        guard let resultViewController = viewController as? T else {
            fatalError("Cannot load view controller from storyboard")
        }
        
        return resultViewController
    }
    
}


extension UIView {
    
    func constrain(in container: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        
        leadingAnchor.constraint(equalTo: container.leadingAnchor).isActive = true
        trailingAnchor.constraint(equalTo: container.trailingAnchor).isActive = true
        topAnchor.constraint(equalTo: container.topAnchor).isActive = true
        bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
    }
    
}
